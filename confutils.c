/*--------------------------------------------------------------------*/
/* functions to connect clients and server */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netdb.h>
#include <time.h> 
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#define MAXNAMELEN 256

/*--------------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/* prepare server to accept requests
 returns file descriptor of socket
 returns -1 on error
 */
int startserver() 
{
	int sd; /* socket descriptor */
	char * servhost; /* full name of this host */
	ushort servport; /* port assigned to this server */
	struct sockaddr_in addr;
	int i;
	struct hostent *host;

	/*
	 FILL HERE
	 create a TCP socket using socket()
	 */
	if((sd = socket(AF_INET,SOCK_STREAM,0)) ==  0)
	{
		perror("Socket Failed\n");
		exit(1);
	}

	/*
	 FILL HERE
	 bind the socket to some port using bind()
	 let the system choose a port
	 */
	memset(&addr, 0 , sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(0);

	if(bind(sd,(struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("Bind AF_INET\n");
		close(sd);
		exit(1);
	}

	/* we are ready to receive connections */

	if(listen(sd, 5) < 0)
	{
		perror("Listen\n");
		exit(1);
	}

	/*
	 FILL HERE
	 figure out the full host name (servhost)
	 use gethostname() and gethostbyname()
	 full host name is remote**.cs.binghamton.edu
	 */

	servhost=(char *)malloc(MAXNAMELEN);

	if( gethostname(servhost, MAXNAMELEN) < 0)
		perror("gethostname() failed\n");


	struct in_addr **addr_list;
	host = gethostbyname(servhost);

	if(host != NULL)
	{
		addr_list = (struct in_addr **)host->h_addr_list;
	}
	else
		perror("servhost\n");
	strcpy(servhost,host->h_name);


	/*
	 FILL HERE
	 figure out the port assigned to this server (servport)
	 use getsockname()
	 */
	int len_inet;
	struct sockaddr_in c_addr;
	int z;

	len_inet = sizeof c_addr;
	c_addr.sin_family = AF_INET;

	if(z = getsockname(sd , (struct sockaddr *) &c_addr , &len_inet) == -1)
	{
		perror("getsockname() failed\n");
		return -1;
	}

	servport = (unsigned) ntohs(c_addr.sin_port);

	/* ready to accept requests */
	printf("admin: started server on '%s' at '%hu'\n\n", servhost, servport);
	free(servhost);
	return (sd);
}
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/*
 establishes connection with the server
 returns file descriptor of socket
 returns -1 on error
 */
int hooktoserver(char *servhost, ushort servport) 
{
	int sd; /* socket descriptor */

	ushort clientport; /* port assigned to this client */
	struct sockaddr_in serv_addr;
	/*
	 FILL HERE
	 create a TCP socket using socket()
	 */
	sd = socket(AF_INET, SOCK_STREAM, 0);
	if(sd < 0)
	{
		printf("Error creating socket\n");
		exit(0);
	}

	/*
	 FILL HERE
	 connect to the server on 'servhost' at 'servport'
	 use gethostbyname() and connect()
	 */
	struct hostent *trans;
	struct sockaddr_in  server;

	if ( (trans = gethostbyname(servhost) ) == NULL ) 
	{
		exit(1);
	}
	memcpy(&server.sin_addr, trans->h_addr_list[0], trans->h_length);
	server.sin_family = AF_INET;
	server.sin_port = htons(servport);

	if ( connect(sd, (struct sockaddr *)&server, sizeof(server) )) 
	{
		exit(1);
	}

	/*
	 FILL HERE
	 figure out the port assigned to this client
	 use getsockname()
	 */
	int len_inet;
	struct sockaddr_in c_addr;
	len_inet = sizeof c_addr;
	int z;

	c_addr.sin_family = AF_INET;

	if(z = getsockname(sd , (struct sockaddr *) &c_addr , &len_inet) == -1)
	{
        	perror("getsockname() failed\n");
        	return -1;
	}

	clientport = (unsigned) ntohs(c_addr.sin_port);

	/* succesful. return socket descriptor */
	printf("admin: connected to server on '%s' at '%hu' thru '%hu'\n", servhost,servport, clientport);
	printf(">");
	fflush(stdout);
	return (sd);
}
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
int readn(int sd, char *buf, int n) 
{
	int toberead;
	char * ptr;

	toberead = n;
	ptr = buf;
	while (toberead > 0) 
	{
		int byteread;

		byteread = read(sd, ptr, toberead);
		if (byteread <= 0)
		{
			if (byteread == -1)
				perror("read");
			return (0);
		}

		toberead -= byteread;
		ptr += byteread;
	}
	return (1);
}

char *recvtext(int sd) 
{
	char *msg;
	long len;

	/* read the message length */
	if (!readn(sd, (char *) &len, sizeof(len))) 
	{
		return (NULL);
	}
	len = ntohl(len);

	/* allocate space for message text */
	msg = NULL;
	if (len > 0) 
	{
		msg = (char *) malloc(len);
		if (!msg) 
		{
			fprintf(stderr, "error : unable to malloc\n");
			return (NULL);
		}

		/* read the message text */
		if (!readn(sd, msg, len)) 
		{
			free(msg);
			return (NULL);
		}
	}

	/* done reading */
	return (msg);
}

int sendtext(int sd, char *msg) 
{
	long len;

	/* write lent */
	len = (msg ? strlen(msg) + 1 : 0);
	len = htonl(len);
	write(sd, (char *) &len, sizeof(len));

	/* write message text */
	len = ntohl(len);
	if (len > 0)
		write(sd, msg, len);
	return (1);
}
/*----------------------------------------------------------------*/

