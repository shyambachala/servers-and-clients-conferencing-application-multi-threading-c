SERVERS AND CLIENTS: CONFERENCING APPLICATION | MULTI-THREADING | C 			 

Project Description

Code Language : C

Created a concurrent multiple client- server chat program using Socket programming on a Linux machine.


IMPLEMENTATION DETAILS:

I have completed the project according to the specifications mentioned in the guidelines of the project.
Both the server and client are working according to the examples given in the template(goodserver and goodclient).
There are no errors and memory leaks when checked using Valgrind tool.
The code is complete.

------------------------------------------------------------------------------

SAMPLE INPUT/OUTPUT:

SERVER:

remote03:~/cn/submit/sbachal1-project1-part1> ./confserver
admin: started server on 'remote03.cs.binghamton.edu' at '50453'

admin: connect from 'remote02.cs.binghamton.edu' at '54188'
admin: connect from 'remote01.cs.binghamton.edu' at '34978'
admin: connect from 'remote04.cs.binghamton.edu' at '47168'
remote02.cs.binghamton.edu(54188): I am user 1
remote01.cs.binghamton.edu(34978): I am user 2
remote04.cs.binghamton.edu(47168): I am user 3
remote02.cs.binghamton.edu(54188): Nic emeetinf you all
^C
remote03:~/cn/dummy/template>
-------------------------------------------------------------------------------
CLIENT-1:

remote02:~/cn/submit/sbachal1-project1-part1> ./confclient remote03.cs.binghamton.edu 50453
admin: connected to server on 'remote03.cs.binghamton.edu' at '50453' thru '54188'
>I am user 1
>>>> I am user 2
>>>> I am user 3
>Nic emeetinf you all
>error: server died
remote02:~/cn/dummy/template>

-------------------------------------------------------------------------------
CLIENT-2:

remote01:~/cn/submit/sbachal1-project1-part1> ./confclient remote03.cs.binghamton.edu 50453
admin: connected to server on 'remote03.cs.binghamton.edu' at '50453' thru '34978'
>>>> I am user 1
>I am user 2
>>>> I am user 3
>>>> Nic emeetinf you all
>error: server died
remote01:~/cn/dummy/template>

-------------------------------------------------------------------------------
CLIENT-3:

remote04:~/cn/submit/sbachal1-project1-part1> ./confclient remote03.cs.binghamton.edu 50453
admin: connected to server on 'remote03.cs.binghamton.edu' at '50453' thru '47168'
>>>> I am user 1
>>>> I am user 2
>I am user 3
>>>> Nic emeetinf you all
>error: server died
remote04:~/cn/dummy/template>

-------------------------------------------------------------------------------
