/*--------------------------------------------------------------------*/
/* conference server */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>

extern int     startserver();
pthread_mutex_t cache_mutex;

char ** cache_request;
char ** cache_response;
char ** cache_ip;
char ** cache_url;
char ** cache_length;
int cache_count = 0;

typedef struct 
{
	int sock;
        struct sockaddr_in sin_cli;
        socklen_t len;
} sock_connection_t;


void* process(void*);
int memory_loan(char **, int, int);
int memory_recover(char **, int);

int main(int argc, char* argv[])
{
	int servsock;
        pthread_t thread;
        sock_connection_t * connection;

    /* check usage */
        if (argc != 1) 
	{
        	fprintf(stderr, "usage : %s\n", argv[0]);
        	exit(1);
        }

        memory_loan(cache_request, 500, 1000);
        memory_loan(cache_response, 500, 1000);
        memory_loan(cache_ip, 500, 50);
        memory_loan(cache_url, 500, 50);
        memory_loan(cache_length, 500, 10);

    /* get ready to receive requests */
        servsock = startserver();
        if (servsock == -1) 
	{
        	perror("Error on starting server: ");
        	exit(1);
        }

        while (1) 
	{
		connection = (sock_connection_t *)malloc(sizeof(sock_connection_t));

       		connection->sock = accept(servsock, (struct sockaddr*)&connection->sin_cli, &connection->len);
        	if (connection->sock<0) 
		{
            		free(connection);
            		continue;
        	}
        	pthread_create(&thread,0, process, (void *)connection);
        	pthread_detach(thread);
    	}
    	memory_recover(cache_request, 500);
    	memory_recover(cache_response, 500);
    	memory_recover(cache_ip, 500);
    	memory_recover(cache_url, 500);
   	memory_recover(cache_length, 500);

}

void* process(void* ptr)
{
    	sock_connection_t * conn;

    	if (!ptr) pthread_exit(0);
    	conn = (sock_connection_t*)ptr;

    	struct hostent *hp;
    	int cli_sd;
    	char cli_ip[256];
    	char cli_length[256];
    	char * clienthost;
   	ushort clientport;
    	int cache_hit = 0, cache_miss = 0;

    	if( getpeername(conn->sock, (struct sockaddr *)&conn->sin_cli, &conn->len) < 0 )
	{
        	perror("Get peername error: ");
        	exit(1);
    	}
    	clientport = ntohs(conn->sin_cli.sin_port);
    	hp = gethostbyaddr(&conn->sin_cli.sin_addr, sizeof(conn->sin_cli.sin_addr), AF_INET);
    	strcpy(cli_ip, inet_ntoa(conn->sin_cli.sin_addr));
	printf("client-ip->%s", cli_ip);

    	if(!hp)
	{
        	perror("Get hostname error: ");
        	exit(1);
    	}
    	clienthost = hp->h_name;
    	printf("admin: connect from '%s' at '%hu'\n", clienthost, clientport);

    	char * msg;
    	long msg_len = 1000;
    	msg = (char *)malloc(msg_len);
printf("1\n");
    	if(read(conn->sock, msg, msg_len) >=0 )
    	{
                char hoststring[5000];
                int i, j, n, flag, portno, sockfd, get_req = 0;
                struct sockaddr_in serv_addr;
                struct in_addr *pptr;
                struct hostent *server;
                char buffer[256];

/*                for(i=0; i<strlen(msg); i++)
                {
                        if(msg[i] == 'G' && msg[i+1] == 'E' && msg[i+2] == 'T')
                        {
                                get_req = 1;
                        }
printf("2\n");
	
			if(!get_req)
			{
				write(conn->sock, "HTTP/1.0 400 BAD REQUEST\n\n", 26);
				goto label;
			}
                }
printf("3\n");
*/

    		printf("%s(%hu): %s", clienthost, clientport, msg);
		if(cache_count != 0)
		{
                for(i=0; i<cache_count; i++)
                {
                        if(strcmp(cache_request[i], msg) == 0)
                        {
                                char cache_dump[5000];
				pthread_mutex_lock(&cache_mutex);
				strcpy(cache_dump, cache_ip[i]);
				strcat(cache_dump, "|");
                                strcat(cache_dump, cache_url[i]);
                                strcat(cache_dump, "|");
                                strcpy(cache_dump, "CACHE_HIT");
                                strcat(cache_dump, "|");
                                strcpy(cache_dump, cache_length[i]);
                                strcat(cache_dump, "|");
                                pthread_mutex_unlock(&cache_mutex);

				write(conn->sock, cache_dump, strlen(cache_dump));
				cache_hit = 1;
                        }
                }
		}
printf("4\n");

		if(cache_hit)
		{
			goto label;
		}
printf("5\n");


		for(i=0; i<strlen(msg); i++)
		{
			if(msg[i] == 'H' && msg[i+1] == 'o' && msg[i+2] == 's' && msg[i+3] == 't')
			{
				for(j=i+6; msg[j] != '\r'; j++)
				{
					hoststring[j-i-6] = msg[j];
				}
				hoststring[j] = '\0';
				break;
			}
		}
printf("6\n");

		pthread_mutex_lock(&cache_mutex);
printf("7\n");
    		strcpy(cache_request[cache_count], msg);
    		strcpy(cache_url[cache_count], hoststring);
    		strcpy(cache_ip[cache_count], cli_ip);
printf("8\n");

		pthread_mutex_unlock(&cache_mutex);
printf("9\n");

		printf("\nHost extracted : '%s'\n", hoststring);
		portno = 80;
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if(sockfd < 0)
		{
			perror("Error opening socket\n");
			exit(1);
		}
		server = gethostbyname(hoststring);
		if (server == NULL)
		{
			fprintf(stderr, "No such host\n");
			exit(0);
		}
		printf("\nConnected to host\n");
	
		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		pptr = (struct in_addr  *)server->h_addr;
		bcopy((char *)pptr, (char *)&serv_addr.sin_addr, server->h_length);
		serv_addr.sin_port = htons(portno);

  /* Connect to server */
		if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0)
		{
			perror("Error in connecting to server\n");
			exit(1);
		}

  /* Message to be sent to the server */
  /* printf("message to server : "); */
		bzero(buffer, 256);
  /* fgets(buffer, 255, stdin); */

  /* Send message to server */
		n = write(sockfd, msg, strlen(msg));
		if(n < 0)
		{
			perror("Error writing to socket\n");
			exit(1);
		}

  /* Read server response */
		bzero(buffer, 256);
		flag = 1;
		printf("\nreading server response\n");
		while( read(sockfd, buffer, 255) > 0)
		{
			printf("%s", buffer);
			write(conn->sock, buffer, strlen(buffer));
		}
printf("10\n");

	        for(i=0; i<strlen(buffer); i++)
                {
                        if(buffer[i] == 'C' && buffer[i+1] == 'o' && buffer[i+2] == 'n' && buffer[i+3] == 't' && buffer[i+4] == 'e' && buffer[i+5] == 'n' && buffer[i+6] == 't' && buffer[i+7] == '-' && buffer[i+8] == 'L')
                        {
                                for(j=i+16; msg[j] != '\r'; j++)
                                {
                                        cli_length[j-i-16] = buffer[j];
                                }
                                cli_length[j] = '\0';
                                break;
                        }
                }
printf("11->Content_length->%s\n", cli_length);

                char core_dump[5000];
                pthread_mutex_lock(&cache_mutex);
		strcpy(core_dump, cli_ip);
		strcat(core_dump, "|");
		strcat(core_dump, hoststring);
                strcat(core_dump, "|");
                strcpy(core_dump, "CACHE_MISS");
                strcat(core_dump, "|");
                strcpy(core_dump, cli_length);
                strcat(core_dump, "|");
                pthread_mutex_unlock(&cache_mutex);
                write(conn->sock, core_dump, strlen(core_dump));

printf("12\n");


		pthread_mutex_lock(&cache_mutex);
		strcpy(cache_response[cache_count], buffer);
		strcpy(cache_length[cache_count], cli_length);
		cache_count++;
        	pthread_mutex_unlock(&cache_mutex);

		close(sockfd);

	}
printf("13\n");

/*	else
	{
		int n;
		n = write(conn->sock, "HTTP/1.0 400 BAD REQUEST\n\n", 26);
		if(n < 0)
		{
			perror("Error writing to socket\n");
		}
	}
*/
printf("14\n");

label:
    	free(msg);
printf("15\n");

}


int memory_loan(char ** buffer, int lines, int length)
{
	int i;
	char ** temp = buffer;
	temp = (char **)malloc(sizeof(char *)*lines);
printf("16\n");

	for(i = 0; i < lines; i++)
	{
		temp[i] = (char *)malloc(sizeof(char)*length);
	}
printf("17\n");

return 0;
}
int memory_recover(char ** buffer, int lines)
{
	int i;
	char ** temp = buffer;
printf("18\n");

	for(i = 0; i < lines; i++)
	{
		free(temp[i]);
	}
printf("19\n");

	free(temp);
printf("20\n");

return 0;
}


