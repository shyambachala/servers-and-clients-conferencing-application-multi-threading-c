/*--------------------------------------------------------------------*/
/* conference server */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netdb.h>
#include <time.h> 
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>

extern char * recvtext(int sd);
extern int sendtext(int sd, char *msg);

extern int startserver();
/*--------------------------------------------------------------------*/

/*--------------------------------------------------------------------*/
int fd_isset(int fd, fd_set *fsp) {
	return FD_ISSET(fd, fsp);
}
/* main routine */
int main(int argc, char *argv[]) 
{
	int servsock; /* server socket descriptor */
	fd_set readsdset, livesdset; /* set of live client sockets */
	int livesdmax; /* largest live client socket descriptor */
	/* check usage */
	if (argc != 1) 
	{
		fprintf(stderr, "usage : %s\n", argv[0]);
		exit(1);
	}

	/* get ready to receive requests */
	servsock = startserver();
	if (servsock == -1) 
	{
		perror("\tError on starting server:\n ");
		exit(1);
	}

	/*
	 FILL HERE:
	 init the set of live clients
	 */
	FD_ZERO(&readsdset);
	FD_ZERO(&livesdset);
	FD_SET(servsock,&livesdset);
	
	livesdmax = servsock;

	/* receive requests and process them */
	while (1) 
	{
		int frsock; /* loop variable */
		readsdset = livesdset;

		/*
		 FILL HERE
		 wait using select() for
		 messages from existing clients and
		 connect requests from new clients
		 */

		if( select(livesdmax + 1,&readsdset,NULL,NULL,NULL) == -1)
		{
			printf("ERROR IN SELECT\n");
			exit(0);
		}



		/* look for messages from live clients */
		for (frsock = 3; frsock <= livesdmax; frsock++) 
		{
			/* skip the listen socket */
			/* this case is covered separately */
			if (frsock == servsock)
				continue;
			if ( fd_isset(frsock, &readsdset)/* FILL HERE: message from client 'frsock'? */ ) 
			{
				char * clienthost; /* host name of the client */
				ushort clientport; /* port number of the client */
				char name_client[100];
		                socklen_t len;
                		struct sockaddr_storage peer_addr;
                		char ip[INET6_ADDRSTRLEN];
                		int port;
                		struct hostent *trans;
                		struct in_addr client_addr;

				/*
				FILL HERE:
				figure out client's host name and port
				using getpeername() and gethostbyaddr()
				*/

				len = sizeof peer_addr;
				getpeername(frsock, (struct sockaddr*)&peer_addr, &len);

				struct sockaddr_in *peer = (struct sockaddr_in *)&peer_addr;

				port = ntohs(peer->sin_port);
				inet_ntop(AF_INET, &peer->sin_addr, ip, sizeof ip);
            			inet_pton(AF_INET, ip, &client_addr);
                		trans = gethostbyaddr(&client_addr, sizeof client_addr, AF_INET);
                		strcpy(name_client,trans->h_name);
                		clientport = port;
				clienthost = name_client;

				char *msg;
				/* read the message */
				msg = recvtext(frsock);
				if (!msg) 
				{
					/* disconnect from client */
					printf("\tadmin: disconnect from '%s(%hu)'\n", clienthost,clientport);
						/*
					 FILL HERE:
					 remove this guy from the set of live clients
					 */
					FD_CLR(frsock,&livesdset);
						/* close the socket */
					close(frsock);
				}
				else 
				{
					/*
					 FILL HERE
					 send the message to all live clients
					 except the one that sent the message
					 */
                    			/* send the message */
					int m;
					for(m = 3; m<=livesdmax; m++)
					{
						if(fd_isset(m,&livesdset))
							{
								if((m != servsock) && (m != frsock))
								{
									sendtext(m,msg);
								}
							}
					}
                    			/* display the message */
                    			printf("%s(%hu): %s", clienthost, clientport, msg);

				}

				/* free the message */
				free(msg);
			}
		}



		/* look for connect requests */
		if ( fd_isset(servsock,&readsdset)/* FILL HERE: connect request from a new client? */ ) 
		{
			/*
			FILL HERE:
			 accept a new connection request
			 */
		        int csd;
        		struct sockaddr_in remoteaddr;
			int size = sizeof remoteaddr;

			csd = accept(servsock,(struct sockaddr *)&remoteaddr,&size);

			/* if accept is fine? */
			if(csd == -1)
			{
				printf("\tERROR: ACCEPT\n");
				exit(0);
			}
			if (csd != -1) 
			{


				char * clienthost; /* host name of the client */
				ushort clientport; /* port number of the client */
				char host_name[100];
                		struct hostent *trans;
                		struct in_addr trans_addr;
                		char ip_addr[100];

				/*
					 FILL HERE:
					 figure out client's host name and port
					 using gethostbyaddr() and without using getpeername().
				 */
				clientport = ntohs(remoteaddr.sin_port);
				strcpy(ip_addr,inet_ntoa(remoteaddr.sin_addr));
				inet_pton(remoteaddr.sin_family, ip_addr, &trans_addr);
				trans = gethostbyaddr(&trans_addr, sizeof trans_addr, remoteaddr.sin_family);
				strcpy(host_name,trans->h_name);
				clienthost = host_name;

				printf("admin: connect from '%s' at '%hu'\n", clienthost,clientport);
					/*
					 FILL HERE:
					 add this guy to set of live clients
					 */
				FD_SET(csd,&livesdset);
				if(csd > livesdmax)
				{
					livesdmax = csd;
				}
			}
		}


	}

	return 0;
}
/*--------------------------------------------------------------------*/

